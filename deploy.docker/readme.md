* **Build Docker container:**
go to the Dockerfile location and run: `docker build --tag provisota/soap-test .`
* **Run Docker container:**
`docker run -it --rm -p 8080:8080 --name soap-test provisota/soap-test`
* **WSDL schema URL:**
http://localhost:8080/soap/soap/webserviceSEI?wsdl