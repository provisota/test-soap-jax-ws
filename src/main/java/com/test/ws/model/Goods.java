package com.test.ws.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Alterovych Ilya
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "goods")
public class Goods implements Serializable {

  private int id;
  private String name;
}
