package com.test.ws.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Alterovych Ilya
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "document")
public class Document implements Serializable {

  private int id;
  private String name;
  @Default
  private List<Goods> goodsList = new ArrayList<>();
}
