package com.test.ws.soap;

import com.test.ws.model.Document;
import com.test.ws.model.Goods;
import java.util.stream.IntStream;
import javax.jws.WebService;

/**
 * @author Alterovych Ilya
 */
@WebService(endpointInterface = "com.test.ws.soap.WebserviceSEI",
    serviceName = "HelloSoap")
public class HelloSoap implements WebserviceSEI {

  @Override
  public String testService() {
    return "Hello from SOAP Webservice!";
  }

  @Override
  public String sayHelloTo(String text) {
    return "Hello to " + text;
  }

  @Override
  public Goods getGoods() {
    return Goods.builder()
        .id(1)
        .name("Some goods test name")
        .build();
  }

  @Override
  public Document getDocument() {
    Document document = Document.builder()
        .id(1)
        .name("Some Document name")
        .build();
    IntStream.rangeClosed(1, 5).forEach(id -> document.getGoodsList().add(
        Goods.builder()
            .id(id)
            .name("Some goods test name")
            .build())
    );
    return document;
  }
}
