package com.test.ws.soap;

import com.test.ws.model.Document;
import com.test.ws.model.Goods;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Alterovych Ilya
 *
 * The @WebService annotation on the implementation class lets CXF know which interface to use when creating WSDL. In this case its simply our
 * HelloWorld interface.
 */
@WebService
public interface WebserviceSEI {

  @WebMethod
    //annotation optional and is mainly used to provide a name attribute to the public method in wsdl
  String testService();

  @WebMethod
  String sayHelloTo(@WebParam(name = "text") String text);

  @WebMethod
  Goods getGoods();

  @WebMethod
  Document getDocument();
}
