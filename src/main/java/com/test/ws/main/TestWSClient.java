package com.test.ws.main;

import com.test.ws.model.Document;
import com.test.ws.model.Goods;
import com.test.ws.soap.WebserviceSEI;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

/**
 * @author Alterovych Ilya
 */
public class TestWSClient {

  public static void main(String[] args) {
    testSOAPFromClient();
  }

  /**
   * create client and test soap service
   */
  private static void testSOAPFromClient() {
    String soapServiceUrl = "http://localhost:8080/soap/webserviceSEI";

    JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
    factoryBean.setServiceClass(WebserviceSEI.class);
    factoryBean.setAddress(soapServiceUrl);

    WebserviceSEI webserviceSEI = (WebserviceSEI) factoryBean.create();

    Goods goods = webserviceSEI.getGoods();
    System.out.println("getGoods() result: " + goods);

    Document document = webserviceSEI.getDocument();
    System.out.println("getDocument() result: " + document);
  }

}
